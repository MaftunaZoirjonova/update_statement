-- Task 1. Alter the rental duration and rental rates of the film you inserted before to three weeks and 9.99, respectively.

UPDATE film
SET rental_duration = 21,
    rental_rate = 9.99
WHERE UPPER(film.title) = UPPER('Red Notice')

-- Task 2. Alter any existing customer in the database with at least 10 rental and 10 payment records. 
-- Change their personal data to yours (first name, last name, address, etc.). 
-- You can use any existing address from the "address" table. 
-- Please do not perform any updates on the "address" table, as this can impact multiple records with the same address.

WITH customer_having_10 AS 
    (SELECT c.customer_id,
        p.payment_count,
        r.rental_count
    FROM customer c
    JOIN (
        SELECT customer_id, COUNT(*) AS payment_count
        FROM payment
        GROUP BY customer_id
        ) p ON c.customer_id = p.customer_id
    JOIN (
        SELECT customer_id, COUNT(*) AS rental_count
        FROM rental
        GROUP BY customer_id
        ) r ON c.customer_id = r.customer_id
    WHERE p.payment_count >= 10 AND r.rental_count >= 10
    LIMIT 1)
UPDATE customer c
SET first_name = 'Maftuna',
    last_name = 'Zoirjonova',
    email = 'zoirjonovamaftuna@gmail.com',
    address_id = (SELECT address_id FROM address LIMIT 1)
FROM customer_having_10
WHERE c.customer_id = customer_having_10.customer_id
RETURNING c.customer_id;

-- Task 3. Change the customer's create_date value to current_date.
-- I took any customer (here it will return customer with customer_id=1)

UPDATE customer
SET create_date = now()
WHERE customer_id = (SELECT customer_id FROM customer LIMIT 1)
RETURNING customer_id, create_date;

-- or if I should update my previus user

UPDATE customer
SET create_date = now()
WHERE first_name = 'Maftuna'
RETURNING customer_id, create_date; 